# React Material-UI for Starter
![Edit Button](https://material-ui.com/static/favicon.ico) 
React components for faster and easier web development. Build your own design system, or start with Material Design

**Examples**
>Are you looking for an example project to get started?
[We host some](https://material-ui.com/getting-started/example-projects/).

**Documentation**
> Check out our [documentation website](https://material-ui.com/).

# Mobx React
[![CircleCI](https://circleci.com/gh/mobxjs/mobx-react.svg?style=svg)](https://circleci.com/gh/mobxjs/mobx-react)
[![Join the chat at https://gitter.im/mobxjs/mobx](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/mobxjs/mobx?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
[![CDNJS](https://img.shields.io/cdnjs/v/mobx-react.svg)](https://cdnjs.com/libraries/mobx-react)

Package with React component wrapper for combining React with MobX.
Exports the `observer` decorator and other utilities.
For documentation, see the [MobX](https://mobxjs.github.io/mobx) project.
There is also work-in-progress [user guide](https://mobx-react.js.org) for additional information.
This package supports both React and React Native.

# React scoped CSS

CSS encapsulation solution for React

## Why

In order to solve the problem of css encapsulation, there are two main approaches, css-modules and css-in-js. However, both of them have a very very big problem. The developer experience is not good, by which I mean you often have to write more code than you expect to achieve a simple style. With react-scoped-css, you can just write the normal css you know, while having the advantage of css encapsulation!

## How does it work

Write your css in a file ends with `.scoped.css` (`scss` & `sass` are also supported)

```css
/* Title.scoped.css */
.title {
  background: #999;
}

p {
  color: #ddd;
}
```

Import the css file

```js
// Title.jsx
import React from 'react'
import './Title.scoped.css'

const Title = props => {
  return (
    <h1 className="title">
      <p>{props.children}</p>
    </h1>
  )
}

export default Title
```

Then, in the html, component with scoped css file imported has a unique `data-v-<hash>` attribute on the html element tag, and the css selector also has a corresponding hash like `selector[data-v-<hash>]`. So all the styles in `Title.scoped.css` are scoped to `Title.jsx`.