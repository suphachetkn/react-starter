const fs = require('fs-extra');
const paths = require('../config/paths');

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
    throw err;
  });

console.log('Clean build dir..');
fs.emptyDirSync(paths.appBuild);
console.log('done');