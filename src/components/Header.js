import React, { Component } from 'react';
import "./styles.scoped.scss";

// using Material UI
import { Grid } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Toolbar from '@material-ui/core/Toolbar';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';

export class Header extends Component {

    render() {
        return (
            <div className="header-layout">
                <Grid container>
                    <Grid className="layout-grid-column" item xs={6}>
                        <Toolbar className="nav-header-left">
                            <div className="search-field">
                                <div className="search-icon">
                                    <SearchIcon />
                                </div>
                                <InputBase
                                    className="seach-input"
                                    placeholder="Search…"
                                    inputProps={{ 'aria-label': 'search' }}
                                />
                            </div>
                        </Toolbar>
                    </Grid>

                    <Grid className="layout-grid-column mat-align-right" item xs={6}>
                        <div className="nav-header-right">
                            <IconButton aria-label="show 4 new mails" color="inherit">
                                <Badge badgeContent={4} color="secondary">
                                    <MailIcon />
                                </Badge>
                            </IconButton>
                            <IconButton aria-label="show 17 new notifications" color="inherit">
                                <Badge badgeContent={17} color="secondary">
                                    <NotificationsIcon />
                                </Badge>
                            </IconButton>
                            <IconButton
                                edge="end"
                                aria-label="account of current user"
                                aria-controls="primary-search-account-menu"
                                aria-haspopup="true"
                                color="inherit"
                            >
                                <AccountCircle />
                            </IconButton>
                        </div>
                    </Grid>
                </Grid>
            </div>
        );
    }
}
