import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import "./styles.scoped.scss";

// MobX React
import { observer } from 'mobx-react';
import MainStore from '../stores/MainStore';

// using Material UI
import Grid from '@material-ui/core/Grid';

// Routes
import routes from '../routes';

// Component
import { Header } from './Header';
import { Footer } from './Footer';
import { Sidebar } from './Sidebar';

@observer
class Layout extends Component {

    getRoutes = () => routes.map((prop) => (
        <Route
            path={prop.path}
            component={prop.component}
            key={prop.key}
        />
    ))

    render() {
        const sideWidth = MainStore.sideBarFolded ? { maxWidth: '70px' } : {}
        const contentWidth = MainStore.sideBarFolded ? {
            maxWidth: 'calc(100% - 70px)',
            flexBasis: '100%'
        } : {}

        return (
            <div className="App-Layout">
                <Grid container>
                    <Grid style={sideWidth} item xs={2}>
                        <Sidebar />
                    </Grid>

                    <Grid className="grid-append-body" style={contentWidth} item xs={10}>
                        <Header />

                        <div className="App-Body-Scale">
                            <Switch>
                                {this.getRoutes()}
                                <Redirect from="/" to="/home" />
                            </Switch>
                        </div>

                        <Footer />
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default Layout;
