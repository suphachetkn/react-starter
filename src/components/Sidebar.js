import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';
import "./styles.scoped.scss";

// using Material-UI
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';

// Constants
import navConstants from '../constants/navigators';

// Third-party
import icons from '../commons/icons';
import MainStore from '../stores/MainStore';

export class Sidebar extends Component {
    state = {
        sideBarFolded: MainStore.sideBarFolded
    }

    foldedMenu = () => {
        this.setState({ sideBarFolded: !MainStore.sideBarFolded })
        MainStore.sideBarFolded = !MainStore.sideBarFolded

        localStorage.setItem("setSidebarFold", MainStore.sideBarFolded ? "active" : "inactive");
    }

    render() {
        const {sideBarFolded} = this.state;

        const sideMenuStyles = sideBarFolded ? { justifyContent: 'center' } : {};
        const toggleStyles = sideBarFolded ? {
            justifyContent: 'center',
            width: '100%'
        } : {};

        return (
            <div className="sidebar-layout">
                <div className="sidebar-header">
                    {!sideBarFolded
                        && <div className="applogo-building">
                            <img src={icons.headerLogo} alt="Material React Starter" />
                            <span>Mat React</span>
                        </div>
                    }

                    <div className="toggle-minimenu" style={toggleStyles} onClick={this.foldedMenu}>
                        {sideBarFolded ? (
                            <CloseIcon />
                        ) : (
                            <MenuIcon />
                        )}
                    </div>
                </div>

                <div className="sidebar-menu">
                {
                    navConstants.mainNav.map((e) =>
                        <NavLink key={e.id} to={e.routes} style={sideMenuStyles} activeClassName="selected">
                            {e.menuIcon}

                            {!sideBarFolded
                                && <span>{e.name}</span>
                            }
                        </NavLink>
                    )
                }
                </div>
            </div>
        );
    }
}
