import React, { Component } from "react";
import "./styles.scoped.scss";

// using Material UI
import Paper from '@material-ui/core/Paper';

// Using Global State
import { observer } from "mobx-react";
import MainStore from "../../stores/MainStore";

@observer
class Home extends Component {

    componentDidMount() {
        document.title = `${MainStore.applicationTitle} | Home`;
    }

    render() {
        return (
            <>
                <p style={{ textAlign: 'center', marginBottom: '2.5em' }}>{MainStore.homeContent}</p>

                <Paper style={{ padding: '1em' }}>
                    <p>Style [SCSS] Scoped same className is : ".spanText"</p>
                    <hr />
                    <p className="spanText">COLOR JUST CHANGE !!!</p>
                </Paper>
            </>
        );
    }
}

export default Home
