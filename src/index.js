import React from 'react';

// Third-party
import ReactDOM from 'react-dom';
import { Switch, BrowserRouter } from 'react-router-dom';

// styles
import './index.scss';

// layouts
import Layout from './components/Layout';

ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Layout />
        </Switch>
      </BrowserRouter>
, document.getElementById('root'));
