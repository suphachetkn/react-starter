// Views
import Home from './view/home/Home';
import About from './view/about/About';
import Contact from './view/contact/Contact';

const routes = [
  {
    key: 'home',
    name: 'Home',
    path: '/home',
    component: Home
  },
  {
    key: 'about',
    name: 'About',
    path: '/about',
    component: About
  },
  {
    key: 'contact',
    name: 'Contact',
    path: '/contact',
    component: Contact
  }
];

export default routes;
