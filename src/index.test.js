import ReactDOM from 'react-dom';

jest.mock('react-dom');

describe('Render application', () => {
  it('Renders the application should successful', () => {
    expect(ReactDOM.render).toBeCalled();
  });
});
