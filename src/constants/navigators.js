import React from 'react';
import HomeIcon from '@material-ui/icons/Home';
import InfoIcon from '@material-ui/icons/Info';
import PermContactCalendarIcon from '@material-ui/icons/PermContactCalendar';

export default {
    mainNav: [
        {
            id: 1,
            name: 'Home',
            menuIcon: <HomeIcon />,
            routes: 'home'
        },
        {
            id: 2,
            name: 'About',
            menuIcon: <InfoIcon />,
            routes: 'about'
        },
        {
            id: 3,
            name: 'Contact',
            menuIcon: <PermContactCalendarIcon />,
            routes: 'contact'
        }
    ],
};
