import { observable } from "mobx"

class MainStore {
    @observable applicationTitle = "React Material";
    @observable sideBarFolded = localStorage.getItem("setSidebarFold") === "active" ? true : false;

    //Example for declare observer variables
    @observable homeContent = `This page {Component} render by "home", Awesome !!!`;
    @observable aboutContent = `This page {Component} render by "about", Awesome !!!`;
    @observable contactContent = `This page {Component} render by "contact", Awesome !!!`;
}


export default new MainStore()
